import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import { DialogTextField } from "./DialogTextFIeld/dialog-text-field";

interface EditDialogProps {
  taskText: string;
  isOpen: boolean;
  closeDialog: () => void;
  editTask: (newTaskText: string) => void;
}

export function EditDialog({
  taskText,
  isOpen,
  closeDialog,
  editTask,
}: EditDialogProps) {
  const [editTaskText, setEditTaskText] = React.useState(taskText);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEditTaskText(event.currentTarget.value);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      handleEnterKey();
    }
  };

  const cancel = () => {
    closeDialog();
    setEditTaskText(taskText);
  };

  const confirm = () => {
    closeDialog();
    editTask(editTaskText);
  };

  const handleEnterKey = () => {
    if (editTaskText) {
      confirm();
    } else {
      cancel();
    }
  };

  return (
    <Dialog open={isOpen} onClose={cancel}>
      <DialogTitle id="form-dialog-title">Edit</DialogTitle>
      <DialogContent>
        <DialogContentText>Press enter to edit.</DialogContentText>
        <DialogTextField
          editTaskText={editTaskText}
          handleInputChange={handleInputChange}
          handleKeyDown={handleKeyDown}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={cancel} color="primary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
}
