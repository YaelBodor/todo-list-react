import React from "react";
import { Checkbox } from "@material-ui/core";
import CircleChecked from "@material-ui/icons/CheckCircleOutline";
import CircleUnchecked from "@material-ui/icons/RadioButtonUnchecked";

interface TodoToggleButtonProps {
  isChecked: boolean;
  handleToggleClick: () => void;
}

export function TodoToggleButton({
  isChecked,
  handleToggleClick,
}: TodoToggleButtonProps) {
  return (
    <Checkbox
      icon={<CircleUnchecked />}
      checkedIcon={<CircleChecked />}
      edge="start"
      checked={isChecked}
      disableRipple
      onClick={handleToggleClick}
    />
  );
}
