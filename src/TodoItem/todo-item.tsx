import React from "react";
import {
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { EditDialog } from "../EditDialog/edit-dialog";
import { useRemoveSign } from "./use-remove-sign";
import { Task } from "../TodosList/todos-list";
import { TodoToggleButton } from "./TodoToggleButton/todo-toggle-button";

const useStyles = makeStyles({
  textFormat: (isChecked: boolean) => {
    return {
      textDecoration: isChecked ? "line-through" : "none",
      color: isChecked ? "#A0A0A0" : "black",
      overflowWrap: "break-word",
    };
  },
});

interface TodoItemProps {
  task: Task;
  handleToggleClick: (toggledTaskId: number) => void;
  removeTask: (taskToRemoveId: number) => () => void;
  editTask: (taskId: number, taskText: string) => void;
}

export function TodoItem({
  task,
  handleToggleClick,
  removeTask,
  editTask,
}: TodoItemProps) {
  const [isRemoveSignShown, showRemoveSign, hideRemoveSign] = useRemoveSign();
  const [isDialogOpen, setIsDialogOpen] = React.useState(false);
  const classes = useStyles(task.isChecked);

  const handleTaskChange = (newTaskText: string) => {
    editTask(task.id, newTaskText);
  };

  const openDialog = () => {
    setIsDialogOpen(true);
    hideRemoveSign();
  };

  return (
    <div onMouseEnter={showRemoveSign} onMouseLeave={hideRemoveSign}>
      <ListItem dense button onDoubleClick={openDialog}>
        <EditDialog
          taskText={task.taskText}
          isOpen={isDialogOpen}
          closeDialog={() => setIsDialogOpen(false)}
          editTask={handleTaskChange}
        />
        <ListItemIcon>
          <TodoToggleButton
            isChecked={task.isChecked}
            handleToggleClick={() => handleToggleClick(task.id)}
          />
        </ListItemIcon>
        <ListItemText primary={task.taskText} className={classes.textFormat} />
        {isRemoveSignShown && (
          <ListItemSecondaryAction>
            <IconButton edge="end" onClick={removeTask(task.id)}>
              <DeleteIcon />
            </IconButton>
          </ListItemSecondaryAction>
        )}
      </ListItem>
    </div>
  );
}
