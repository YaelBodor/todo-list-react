import React from "react";
import { Divider, List, makeStyles } from "@material-ui/core";
import { InputField } from "../InputField/input-field";
import { TodoItem } from "../TodoItem/todo-item";
import { ListActions } from "../ListActions/list-actions";

const ALL_TASKS_FILTER = "allTasks";
const ACTIVE_TASKS_FILTER = "activeTasks";
const COMPLETED_TASKS_FILTER = "completedTasks";

const useStyles = makeStyles({
  listSize: {
    width: "35vw",
  },
});

export interface Task {
  id: number;
  taskText: string;
  isChecked: boolean;
}

export function TodosList() {
  const classes = useStyles();
  const [todos, setTodos] = React.useState([
    { id: 0, taskText: "Go shopping", isChecked: false },
    { id: 1, taskText: "Clean", isChecked: false },
    { id: 2, taskText: "Write a blog", isChecked: false },
    { id: 3, taskText: "Call grandma", isChecked: false },
  ]);
  const [typeOfTasksToDisplay, setTypeOfTasksToDisplay] = React.useState(
    ALL_TASKS_FILTER
  );
  const TASKS_BY_TYPE: {
    [index: string]: Task[];
    [ALL_TASKS_FILTER]: Task[];
    [ACTIVE_TASKS_FILTER]: Task[];
    [COMPLETED_TASKS_FILTER]: Task[];
  } = {
    [ALL_TASKS_FILTER]: [...todos],
    [ACTIVE_TASKS_FILTER]: [...todos].filter((task) => !task.isChecked),
    [COMPLETED_TASKS_FILTER]: [...todos].filter((task) => task.isChecked),
  };

  const handleToggleClick = (toggledTaskId: number) => {
    setTodos(
      todos.map((task) => {
        if (task.id === toggledTaskId) {
          return {
            ...task,
            isChecked: !task.isChecked,
          };
        }
        return task;
      })
    );
  };

  const addNewTask = (newTaskText: string) => {
    const newTask = {
      id: todos[todos.length - 1].id + 1,
      taskText: newTaskText,
      isChecked: false,
    };
    setTodos([...todos, newTask]);
  };

  const removeTask = (taskToRemoveId: number) => () => {
    setTodos(todos.filter((task) => task.id !== taskToRemoveId));
  };

  const removeAllCompletedTasks = () => {
    setTodos(todos.filter((task) => task.isChecked === false));
  };

  const editTask = (taskId: number, taskText: string) => {
    setTodos(
      todos.map((task) => (task.id === taskId ? { ...task, taskText } : task))
    );
  };

  const amountOfActiveTasks = () =>
    todos.filter((task) => !task.isChecked).length;

  const todoItems = () =>
    TASKS_BY_TYPE[typeOfTasksToDisplay].map((task) => (
      <TodoItem
        key={task.id}
        task={task}
        handleToggleClick={handleToggleClick}
        removeTask={removeTask}
        editTask={editTask}
      />
    ));

  const filtersButtons = [
    {
      filter: "All",
      setTasksFiler: () => setTypeOfTasksToDisplay(ALL_TASKS_FILTER),
    },
    {
      filter: "Active",
      setTasksFiler: () => setTypeOfTasksToDisplay(ACTIVE_TASKS_FILTER),
    },
    {
      filter: "Completed",
      setTasksFiler: () => setTypeOfTasksToDisplay(COMPLETED_TASKS_FILTER),
    },
  ];

  return (
    <List className={classes.listSize}>
      <InputField onKeyDown={addNewTask} />
      {todoItems()}
      <Divider />
      <ListActions
        amountOfLeftTasks={amountOfActiveTasks()}
        removeAllCompletedTasks={removeAllCompletedTasks}
        filters={filtersButtons}
      />
    </List>
  );
}
