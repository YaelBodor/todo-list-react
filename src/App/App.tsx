import "fontsource-roboto";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { TodosList } from "../TodosList/todos-list";

const useStyles = makeStyles({
  center: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
});

function App() {
  const classes = useStyles();

  return (
    <div className={classes.center}>
      <Typography color="secondary" variant="h1">
        Todos
      </Typography>
      <TodosList />
    </div>
  );
}

export default App;
