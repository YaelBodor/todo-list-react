import React from "react";

export function useRemoveSign(): [boolean, () => void, () => void] {
  const [isRemoveSignShown, setIsRemoveSignShown] = React.useState(false);

  const showRemoveSign = () => setIsRemoveSignShown(true);
  const hideRemoveSign = () => setIsRemoveSignShown(false);

  return [isRemoveSignShown, showRemoveSign, hideRemoveSign];
}
