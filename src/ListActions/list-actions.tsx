import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { ButtonGroup, Button, Typography, Grid } from "@material-ui/core";

const useStyles = makeStyles({
  space: {
    marginTop: "5%",
  },
});

interface ListActionsProps {
  amountOfLeftTasks: number;
  removeAllCompletedTasks: () => void;
  filters: {
    filter: string;
    setTasksFiler: () => void;
  }[];
}

export function ListActions({
  amountOfLeftTasks,
  removeAllCompletedTasks,
  filters,
}: ListActionsProps) {
  const classes = useStyles();

  return (
    <Grid
      container
      spacing={1}
      alignItems="center"
      justify="space-between"
      className={classes.space}
    >
      <Grid item>
        <Typography variant="caption">
          {`${amountOfLeftTasks} items left`}
        </Typography>
      </Grid>
      <Grid item>
        <ButtonGroup size="small">
          {filters.map((filter) => {
            return (
              <Button key={filter.filter} onClick={filter.setTasksFiler}>
                {filter.filter}
              </Button>
            );
          })}
        </ButtonGroup>
      </Grid>
      <Grid item>
        <Button
          size="small"
          color="secondary"
          onClick={removeAllCompletedTasks}
        >
          Clear Completed
        </Button>
      </Grid>
    </Grid>
  );
}
