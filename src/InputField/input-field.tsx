import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  fullWidth: {
    width: "fill-available",
  },
});

interface InputFieldProps {
  onKeyDown: (newTask: string) => void;
}

export function InputField({ onKeyDown }: InputFieldProps) {
  const [newTaskText, setNewTaskText] = React.useState("");
  const classes = useStyles();

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNewTaskText(event.currentTarget.value);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key !== "Enter") {
      return;
    }

    if (newTaskText.trim().length !== 0) {
      onKeyDown(newTaskText);
    }
    setNewTaskText("");
  };

  return (
    <TextField
      label="What needs to be done ?"
      id="outlined-start-adornment"
      className={classes.fullWidth}
      variant="outlined"
      value={newTaskText}
      onKeyDown={handleKeyDown}
      onChange={handleInputChange}
    />
  );
}
