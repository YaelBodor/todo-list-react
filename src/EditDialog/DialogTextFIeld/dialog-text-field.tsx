import React from "react";
import { TextField, InputAdornment } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";

interface DialogTextFieldProps {
  editTaskText: string;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleKeyDown: (event: React.KeyboardEvent<HTMLInputElement>) => void;
}

export function DialogTextField({
  editTaskText,
  handleInputChange,
  handleKeyDown,
}: DialogTextFieldProps) {
  return (
    <TextField
      autoFocus
      margin="dense"
      id="name"
      type="text"
      fullWidth
      value={editTaskText}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <EditIcon />
          </InputAdornment>
        ),
      }}
      onChange={handleInputChange}
      onKeyDown={handleKeyDown}
    />
  );
}
